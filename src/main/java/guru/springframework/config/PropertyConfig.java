package guru.springframework.config;

import guru.springframework.beanExamples.FakeDataSource;
import guru.springframework.beanExamples.FakeJmsBroker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource({"classpath:datasource.properties",
        "classpath:jms.properties"})
//@PropertySources...
public class PropertyConfig {

    @Autowired
    Environment env;

    @Value("${geek.username}")
    String user;

    @Value("${geek.password}")
    String password;

    @Value("${geek.url}")
    String url;

    @Value("${geek.jms.username}")
    String jmsUsername;

    @Value("${geek.jms.username")
    String jmsPassword;

    @Value("${geek.jms.url}")
    String jmsUrl;

    @Bean
    public FakeJmsBroker fakeJmsBroker(){
        FakeJmsBroker jmsBroker = new FakeJmsBroker();
        jmsBroker.setUsername(jmsUsername);
        jmsBroker.setPassword(jmsPassword);
        jmsBroker.setUrl(jmsUrl);
        return jmsBroker;
    }

    @Bean
    public FakeDataSource fakeDataSource(){
        FakeDataSource fakeDataSource = new FakeDataSource(env.getProperty("USERNAME"), password, url);
        return fakeDataSource;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer properties(){
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        return propertySourcesPlaceholderConfigurer;
    }

}
